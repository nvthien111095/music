<?php
define('FS_METHOD', 'direct');
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_music' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Q/C(LdtDASsA/|av pPT5=YFt3jbzI{:M|aR7./bO&y<{- D3jCtX/4W@R@mi|aJ' );
define( 'SECURE_AUTH_KEY',  'lh;&OTIwzAXIyH}NZ&hB72f%kmA305sU$|uNa2#3kipfEMT:RBhplcw@(4#VXK<V' );
define( 'LOGGED_IN_KEY',    '$#*C@,%M47E0`IY|SVlRSE4!~u9nCK7]@N,Um62)<N*tH7%ha$i?T/y :HmGsGzj' );
define( 'NONCE_KEY',        '|tT[BETATV@~Lx|W*GW7_3~ fHl^4~k`&a#F4|SM_#Un3R*HL[J}a }GG+N={qdW' );
define( 'AUTH_SALT',        '04$_uGvemTB&Km2Sj3c%$BAu2czbV<mOiL#YC/!;l!:4M7`Y{kX1c)*do9=2=C)_' );
define( 'SECURE_AUTH_SALT', 'Y>!tA|UE>bEKgfL{uMQ,)@$NQE.4Z(vi+gU[p1`!pSdpv}.Vy!kRu~1={]:_tu;d' );
define( 'LOGGED_IN_SALT',   '*gv.#7(rNwc#MX<=x+p>9<ELvRTd<VXSRuE%Z9wH0!B.Ch_t7>Rd2.`OKI}o{ZEK' );
define( 'NONCE_SALT',       '$xwRMyfLI{DcfU^.:!Ir tix,(a@Tt_=9Yb WQg[JJ3R<l+]C=]@*z3i&D?Gigdb' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
